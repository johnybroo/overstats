package com.datoverbase.tpi_ip.notoverbase.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.datoverbase.tpi_ip.notoverbase.API.ClientServiceGenerator;
import com.datoverbase.tpi_ip.notoverbase.API.OverwatchAPI;
import com.datoverbase.tpi_ip.notoverbase.Activities.DetailHeroActivity;
import com.datoverbase.tpi_ip.notoverbase.Data.DataManager;
import com.datoverbase.tpi_ip.notoverbase.Data.Hero.HeroData;
import com.datoverbase.tpi_ip.notoverbase.Data.Hero.HeroDetailData;
import com.datoverbase.tpi_ip.notoverbase.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HeroesAdapter extends RecyclerView.Adapter<HeroesAdapter.ViewHolder> {

    Context context;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View heroView = inflater.inflate(R.layout.hero_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(heroView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        HeroData heroData = DataManager.getInstance().getHeroeasData().get(position);
        holder.heroTitle.setText(heroData.getName());
        holder.heroDescription.setText(heroData.getReal_name());
        int id = context.getResources().getIdentifier("h" + position,"drawable", context.getPackageName());
        Drawable drawable = context.getResources().getDrawable(id);
        holder.heroImage.setImageDrawable(drawable);
    }

    @Override
    public int getItemCount() {
        return DataManager.getInstance().getHeroeasData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView heroImage;
        TextView heroTitle;
        TextView heroDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            heroImage = (ImageView)itemView.findViewById(R.id.heroImage);
            heroTitle = (TextView)itemView.findViewById(R.id.heroTitle);
            heroDescription = (TextView)itemView.findViewById(R.id.mapDescription);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OverwatchAPI clientOverwatchAPI = ClientServiceGenerator.createService(OverwatchAPI.class, OverwatchAPI.BASE_URL);

                    clientOverwatchAPI.getHeroID(getAdapterPosition()+1).enqueue(new Callback<HeroDetailData>() {
                        @Override
                        public void onResponse(Call<HeroDetailData> call, Response<HeroDetailData> response) {
                            Intent intent = new Intent(context, DetailHeroActivity.class);
                            intent.putExtra("heroData", response.body());
                            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity)context, heroImage, "heroImageTransition");
                            context.startActivity(intent, options.toBundle());
                        }

                        @Override
                        public void onFailure(Call<HeroDetailData> call, Throwable t) {

                        }
                    });
                }
            });
        }
    }
}
