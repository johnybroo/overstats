package com.datoverbase.tpi_ip.notoverbase.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.datoverbase.tpi_ip.notoverbase.API.ClientServiceGenerator;
import com.datoverbase.tpi_ip.notoverbase.API.OverwatchAPI;
import com.datoverbase.tpi_ip.notoverbase.Activities.DetailMapActivity;
import com.datoverbase.tpi_ip.notoverbase.Data.DataManager;
import com.datoverbase.tpi_ip.notoverbase.Data.Map.MapData;
import com.datoverbase.tpi_ip.notoverbase.Data.Map.MapDetailData;
import com.datoverbase.tpi_ip.notoverbase.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsAdapter extends RecyclerView.Adapter<MapsAdapter.ViewHolder> {

    Context context;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View HeroView = inflater.inflate(R.layout.map_row, parent, false);

        ViewHolder viewHolder = new ViewHolder(HeroView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MapData mapData = DataManager.getInstance().getMapsData().get(position);
        holder.mapTitle.setText(mapData.getName());
        holder.mapDescription.setText(mapData.getLocation());
        int id = context.getResources().getIdentifier("m" + position,"drawable", context.getPackageName());
        Drawable drawable = context.getResources().getDrawable(id);
        holder.mapImage.setImageDrawable(drawable);
    }

    @Override
    public int getItemCount() {
        return DataManager.getInstance().getMapsData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView mapImage;
        TextView mapTitle;
        TextView mapDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            mapImage = (ImageView)itemView.findViewById(R.id.mapImage);
            mapTitle = (TextView)itemView.findViewById(R.id.mapTitle);
            mapDescription = (TextView)itemView.findViewById(R.id.mapDescription);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OverwatchAPI clientOverwatchAPI = ClientServiceGenerator.createService(OverwatchAPI.class, OverwatchAPI.BASE_URL);

                    clientOverwatchAPI.getMapID(getAdapterPosition()+1).enqueue(new Callback<MapDetailData>() {
                        @Override
                        public void onResponse(Call<MapDetailData> call, Response<MapDetailData> response) {
                            Intent intent = new Intent(context, DetailMapActivity.class);
                            intent.putExtra("mapData", response.body());
                            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity)context, mapImage, "mapImageTransition");
                            context.startActivity(intent, options.toBundle());
                        }

                        @Override
                        public void onFailure(Call<MapDetailData> call, Throwable t) {

                        }
                    });
                }
            });
        }
    }
}
