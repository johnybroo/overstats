package com.datoverbase.tpi_ip.notoverbase.Data;

import android.os.Parcel;
import android.os.Parcelable;

public class RoleData implements Parcelable {
    private int id;
    private String name;

    public RoleData(Parcel in) {
        id = in.readInt();
        name = in.readString();
    }

    public RoleData(){}

    public static final Creator<RoleData> CREATOR = new Creator<RoleData>() {
        @Override
        public RoleData createFromParcel(Parcel in) {
            return new RoleData(in);
        }

        @Override
        public RoleData[] newArray(int size) {
            return new RoleData[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
    }
}
