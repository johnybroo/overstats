package com.datoverbase.tpi_ip.notoverbase.Data.Player;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class Us implements Parcelable
{
    private Stats stats;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    public final static Creator<Us> CREATOR = new Creator<Us>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Us createFromParcel(Parcel in) {
            Us instance = new Us();
            instance.stats = ((Stats) in.readValue((Stats.class.getClassLoader())));
            instance.additionalProperties = ((Map<String, Object> ) in.readValue((Map.class.getClassLoader())));
            return instance;
        }

        public Us[] newArray(int size) {
            return (new Us[size]);
        }

    }
            ;

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(stats);
        dest.writeValue(additionalProperties);
    }

    public int describeContents() {
        return 0;
    }

}
