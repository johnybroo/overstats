package com.datoverbase.tpi_ip.notoverbase.Data.PatchNote;

import java.util.List;

public class PatchNoteResponse {
    private List<PatchNoteData> patchNotes = null;

    public List<PatchNoteData> getPatchNotes() {
        return patchNotes;
    }

    public void setPatchNotes(List<PatchNoteData> patchNotes) {
        this.patchNotes = patchNotes;
    }
}
