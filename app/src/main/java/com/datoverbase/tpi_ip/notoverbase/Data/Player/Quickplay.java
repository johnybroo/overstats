package com.datoverbase.tpi_ip.notoverbase.Data.Player;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class Quickplay implements Parcelable
{

    private Overall_stats overall_stats;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    public final static Creator<Quickplay> CREATOR = new Creator<Quickplay>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Quickplay createFromParcel(Parcel in) {
            Quickplay instance = new Quickplay();
            instance.overall_stats = ((Overall_stats) in.readValue((Overall_stats.class.getClassLoader())));
            instance.additionalProperties = ((Map<String, Object> ) in.readValue((Map.class.getClassLoader())));
            return instance;
        }

        public Quickplay[] newArray(int size) {
            return (new Quickplay[size]);
        }

    }
            ;

    public Overall_stats getOverall_stats() {
        return overall_stats;
    }

    public void setOverall_stats(Overall_stats Overall_stats) {
        this.overall_stats = Overall_stats;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(overall_stats);
        dest.writeValue(additionalProperties);
    }

    public int describeContents() {
        return 0;
    }

}
