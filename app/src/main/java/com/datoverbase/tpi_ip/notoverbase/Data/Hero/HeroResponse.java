package com.datoverbase.tpi_ip.notoverbase.Data.Hero;

import java.util.List;

public class HeroResponse {
    private List<HeroData> data = null;

    public List<HeroData> getData() {
        return data;
    }

    public void setData(List<HeroData> data) {
        this.data = data;
    }
}
