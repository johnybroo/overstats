package com.datoverbase.tpi_ip.notoverbase.Data.Map;

import android.os.Parcel;
import android.os.Parcelable;

public class MapModeData implements Parcelable{
    private int id;
    private String name;

    protected MapModeData(Parcel in) {
        id = in.readInt();
        name = in.readString();
    }

    public static final Creator<MapModeData> CREATOR = new Creator<MapModeData>() {
        @Override
        public MapModeData createFromParcel(Parcel in) {
            return new MapModeData(in);
        }

        @Override
        public MapModeData[] newArray(int size) {
            return new MapModeData[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
    }
}
