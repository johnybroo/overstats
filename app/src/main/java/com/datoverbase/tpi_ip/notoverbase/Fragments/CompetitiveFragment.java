package com.datoverbase.tpi_ip.notoverbase.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.datoverbase.tpi_ip.notoverbase.Data.DataManager;
import com.datoverbase.tpi_ip.notoverbase.Data.Player.Competitive;
import com.datoverbase.tpi_ip.notoverbase.R;
import com.squareup.picasso.Picasso;

public class CompetitiveFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.competitive_fragment, container, false);
        Competitive stats = DataManager.getInstance().getPlayerStats().getCompetitive();

        String RankImage = String.valueOf(stats.getOverall_stats().getAvatar());
        ImageView iv_rank = (ImageView)v.findViewById(R.id.user_portrait);
        Picasso.with(getContext()).load(RankImage).into(iv_rank);

        int prestige = stats.getOverall_stats().getPrestige();
        int levelbrut = stats.getOverall_stats().getLevel();
        int level = (prestige * 100) + levelbrut;

        TextView tv_lvl = (TextView)v.findViewById(R.id.lvl_txt);
        tv_lvl.setText(String.valueOf(" " + level + " "));

        TextView tv_Tier = (TextView)v.findViewById(R.id.tier_txt);
        tv_Tier.setText(String.valueOf(stats.getOverall_stats().getTier()));

        TextView tv_Nickname = (TextView)v.findViewById(R.id.Pseudo);
        tv_Nickname.setText(DataManager.getInstance().getPseudo());

        TextView tv_Rank = (TextView)v.findViewById(R.id.rank_txt);
        tv_Rank.setText(String.valueOf(stats.getOverall_stats().getComprank()));

        TextView tv_games = (TextView)v.findViewById(R.id.games_txt);
        tv_games.setText((String.valueOf(stats.getOverall_stats().getGames())));

        TextView tv_victories = (TextView)v.findViewById(R.id.victories_txt);
        tv_victories.setText(String.valueOf(stats.getOverall_stats().getWins()));
        TextView tv_defeats = (TextView)v.findViewById(R.id.defeats_txt);
        tv_defeats.setText(String.valueOf(stats.getOverall_stats().getLosses()));
        TextView tv_ties = (TextView)v.findViewById(R.id.ties_txt);
        tv_ties.setText(String.valueOf(stats.getOverall_stats().getTies()));

        TextView tv_winrate = (TextView)v.findViewById(R.id.winrate_txt);
        tv_winrate.setText((String.valueOf(stats.getOverall_stats().getWin_rate() + "%")));

        return v;
    }
}
