package com.datoverbase.tpi_ip.notoverbase.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.datoverbase.tpi_ip.notoverbase.API.ClientServiceGenerator;
import com.datoverbase.tpi_ip.notoverbase.API.OWAPI;
import com.datoverbase.tpi_ip.notoverbase.Data.DataManager;
import com.datoverbase.tpi_ip.notoverbase.Data.Player.PlayerStats;
import com.datoverbase.tpi_ip.notoverbase.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar tb;
    ProgressDialog progressDialog;
    EditText etBtag;
    RadioButton rbEU, rbUS, rbKR;
    String region = "EU";
    PlayerStats playerStats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        tb = (Toolbar) findViewById(R.id.tb);
        setSupportActionBar(tb);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rbEU = (RadioButton) findViewById(R.id.rbEU);
        rbUS = (RadioButton) findViewById(R.id.rbUS);
        rbKR = (RadioButton) findViewById(R.id.rbKR);

        rbEU.setOnClickListener(this);
        rbUS.setOnClickListener(this);
        rbKR.setOnClickListener(this);

        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Retrieving data ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        etBtag = (EditText) findViewById(R.id.etBattleTag);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        final Button btn_login = (Button) findViewById(R.id.btn_login);

        btn_login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if(!checkBattleTag(etBtag.getText().toString()))
                {
                    alertDialogBuilder.setMessage("Please enter a valid BattleTag");
                    alertDialogBuilder.create().show();

                }
                else
                {
                    progressDialog.show();

                    OWAPI clientOWAPI = ClientServiceGenerator.createService(OWAPI.class, OWAPI.BASE_URL);
                    clientOWAPI.getPlayerStats(etBtag.getText().toString()).enqueue(new Callback<PlayerStats>() {
                        @Override
                        public void onResponse(Call<PlayerStats> call, Response<PlayerStats> response) {
                            progressDialog.dismiss();
                            playerStats = response.body();
                            if (playerStats == null)
                            {
                                alertDialogBuilder.setMessage("Cannot retrieve data for this user");
                                alertDialogBuilder.create().show();
                                return;
                            }


                            DataManager.getInstance().setPseudo(etBtag.getText().toString().split("-")[0]);

                            switch (region)
                            {
                                case "EU":
                                    if (playerStats.getEu() != null)
                                    {
                                        DataManager.getInstance().setPlayerStats(playerStats.getEu().getStats());
                                        Intent intent = new Intent(getApplicationContext(), StatisticsActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                    else
                                    {
                                        alertDialogBuilder.setMessage("You haven't played on this server yet");
                                        alertDialogBuilder.create().show();
                                    }
                                    break;

                                case "US":
                                    if (playerStats.getUs() != null)
                                    {
                                        DataManager.getInstance().setPlayerStats(playerStats.getUs().getStats());
                                        Intent intent = new Intent(getApplicationContext(), StatisticsActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                    else
                                    {
                                        alertDialogBuilder.setMessage("You haven't played on this server yet");
                                        alertDialogBuilder.create().show();
                                    }
                                    break;

                                case "KR":
                                    if (playerStats.getKr() != null)
                                    {
                                        DataManager.getInstance().setPlayerStats(playerStats.getKr().getStats());
                                        Intent intent = new Intent(getApplicationContext(), StatisticsActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                    else
                                    {
                                        alertDialogBuilder.setMessage("You haven't played on this server yet");
                                        alertDialogBuilder.create().show();
                                    }
                                    break;
                            }


                        }

                        @Override
                        public void onFailure(Call<PlayerStats> call, Throwable t) {
                            progressDialog.dismiss();
                            alertDialogBuilder.setMessage("Cannot retrieve data for this user");
                            alertDialogBuilder.create().show();
                        }
                    });
                }
            }
        });
    }

    private boolean checkBattleTag(String s)
    {
        Pattern pattern = Pattern.compile("^[a-zA-Z]+-[0-9]+$");
        Matcher matcher = pattern.matcher(s);
        return  matcher.matches();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.rbEU:
                region = "EU";
                break;

            case R.id.rbUS:
                region = "US";
                break;

            case R.id.rbKR:
                region = "KR";
                break;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}