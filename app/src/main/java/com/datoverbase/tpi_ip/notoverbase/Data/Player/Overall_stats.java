package com.datoverbase.tpi_ip.notoverbase.Data.Player;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class Overall_stats implements Parcelable
{
    private float win_rate;
    private String avatar;
    private int losses;
    private int prestige;
    private int wins;
    private String rank_image;
    private int ties;
    private int games;
    private String tier;
    private int level;
    private int comprank;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    public final static Creator<Overall_stats> CREATOR = new Creator<Overall_stats>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Overall_stats createFromParcel(Parcel in) {
            Overall_stats instance = new Overall_stats();
            instance.win_rate = ((float) in.readValue((float.class.getClassLoader())));
            instance.avatar = ((String) in.readValue((String.class.getClassLoader())));
            instance.losses = ((int) in.readValue((int.class.getClassLoader())));
            instance.prestige = ((int) in.readValue((int.class.getClassLoader())));
            instance.wins = ((int) in.readValue((int.class.getClassLoader())));
            instance.rank_image = ((String) in.readValue((String.class.getClassLoader())));
            instance.ties = ((int) in.readValue((int.class.getClassLoader())));
            instance.games = ((int) in.readValue((int.class.getClassLoader())));
            instance.tier = ((String) in.readValue((String.class.getClassLoader())));
            instance.level = ((int) in.readValue((int.class.getClassLoader())));
            instance.comprank = ((int) in.readValue((int.class.getClassLoader())));
            instance.additionalProperties = ((Map<String, Object> ) in.readValue((Map.class.getClassLoader())));
            return instance;
        }

        public Overall_stats[] newArray(int size) {
            return (new Overall_stats[size]);
        }

    }
            ;

    public float getWin_rate() {
        return win_rate;
    }

    public void setWin_rate(float win_rate) {
        this.win_rate = win_rate;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public int getPrestige() {
        return prestige;
    }

    public void setPrestige(int prestige) {
        this.prestige = prestige;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public String getRank_image() {
        return rank_image;
    }

    public void setRank_image(String rank_image) {
        this.rank_image = rank_image;
    }

    public int getTies() {
        return ties;
    }

    public void setTies(int ties) {
        this.ties = ties;
    }

    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getComprank() {
        return comprank;
    }

    public void setComprank(int comprank) {
        this.comprank = comprank;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(win_rate);
        dest.writeValue(avatar);
        dest.writeValue(losses);
        dest.writeValue(prestige);
        dest.writeValue(wins);
        dest.writeValue(rank_image);
        dest.writeValue(ties);
        dest.writeValue(games);
        dest.writeValue(tier);
        dest.writeValue(level);
        dest.writeValue(comprank);
        dest.writeValue(additionalProperties);
    }

    public int describeContents() {
        return 0;
    }

}
