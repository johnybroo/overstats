package com.datoverbase.tpi_ip.notoverbase.Activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.datoverbase.tpi_ip.notoverbase.Adapters.StatsAdapter;
import com.datoverbase.tpi_ip.notoverbase.Data.DataManager;
import com.datoverbase.tpi_ip.notoverbase.Data.Player.Stats;
import com.datoverbase.tpi_ip.notoverbase.R;

public class StatisticsActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tl;
    Toolbar tb;
    Stats playerStats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics);

        playerStats = DataManager.getInstance().getPlayerStats(); //Récupération des statistiques du joueurs

        tb = (Toolbar) findViewById(R.id.tb);
        setSupportActionBar(tb);

        setSupportActionBar(tb);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.vp);
        StatsAdapter MyAdapter = new StatsAdapter(getSupportFragmentManager());
        viewPager.setAdapter(MyAdapter);

        tl = (TabLayout) findViewById(R.id.tl);
        tl.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}