package com.datoverbase.tpi_ip.notoverbase.Data.Map;

import android.os.Parcel;
import android.os.Parcelable;

public class MapDetailData implements Parcelable {
    private int id;
    private String name;
    private String location;
    private MapModeData mode;

    public MapDetailData(Parcel in) {
        id = in.readInt();
        name = in.readString();
        location = in.readString();
        mode = in.readParcelable(MapModeData.class.getClassLoader());
    }

    public MapDetailData(){}

    public static final Creator<MapDetailData> CREATOR = new Creator<MapDetailData>() {
        @Override
        public MapDetailData createFromParcel(Parcel in) {
            return new MapDetailData(in);
        }

        @Override
        public MapDetailData[] newArray(int size) {
            return new MapDetailData[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public MapModeData getMode() {
        return mode;
    }

    public void setMode(MapModeData mode) {
        this.mode = mode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(location);
        dest.writeParcelable(mode, flags);
    }
}
