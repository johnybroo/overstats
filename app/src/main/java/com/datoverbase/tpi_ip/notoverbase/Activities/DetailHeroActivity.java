package com.datoverbase.tpi_ip.notoverbase.Activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.datoverbase.tpi_ip.notoverbase.Data.Hero.HeroDetailData;
import com.datoverbase.tpi_ip.notoverbase.R;

public class DetailHeroActivity extends AppCompatActivity {

    Toolbar tb;
    HeroDetailData heroData;
    TextView tvh_name;
    TextView tvh_description;
    TextView tvh_affiliation;
    TextView tvh_role;
    TextView tvh_difficulty;
    TextView tvh_health;
    TextView tvh_armor;
    TextView tvh_shield;
    TextView tvh_realname;
    TextView tvh_age;
    TextView tvh_base;
    TextView tvh_nameAbility0;
    TextView tvh_nameAbility1;
    TextView tvh_nameAbility2;
    TextView tvh_nameAbility3;
    TextView tvh_nameAbility4;
    TextView tvh_ability0;
    TextView tvh_ability1;
    TextView tvh_ability2;
    TextView tvh_ability3;
    TextView tvh_ability4;
    ImageView hero_portrait;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hero);

        heroData = getIntent().getParcelableExtra("heroData"); //Recuperation des info du hero clické

        tb = (Toolbar) findViewById(R.id.tb);
        setSupportActionBar(tb);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setHeroData();
    }

    private void setHeroData()
    {
        tvh_name = (TextView)findViewById(R.id.mapName);
        tvh_name.setText(heroData.getName());

        tvh_description = (TextView)findViewById(R.id.mapDescription);
        tvh_description.setText(heroData.getDescription());

        tvh_affiliation = (TextView)findViewById(R.id.heroAffiliation);
        tvh_affiliation.setText(heroData.getAffiliation());

        tvh_role = (TextView)findViewById(R.id.heroRole);
        tvh_role.setText(heroData.getRole().getName());


        tvh_difficulty = (TextView)findViewById(R.id.heroDifficulty);
        tvh_difficulty.setText(String.valueOf(heroData.getDifficulty()));

        tvh_health = (TextView)findViewById(R.id.heroHealth);
        tvh_health.setText(String.valueOf(heroData.getHealth()));

        tvh_armor = (TextView)findViewById(R.id.heroArmor);
        tvh_armor.setText(String.valueOf(heroData.getArmour()));

        tvh_shield = (TextView)findViewById(R.id.heroShield);
        tvh_shield.setText(String.valueOf(heroData.getShield()));

        tvh_realname = (TextView)findViewById(R.id.heroRealName);
        tvh_realname.setText(heroData.getReal_name());

        tvh_age = (TextView)findViewById(R.id.heroAge);
        tvh_age.setText(String.valueOf(heroData.getAge()));

        tvh_base = (TextView)findViewById(R.id.heroBaseOperations);
        tvh_base.setText(heroData.getBase_of_operations());

        tvh_nameAbility0 = (TextView)findViewById(R.id.abilityName0);
        tvh_nameAbility0.setText(heroData.getAbilities().get(0).getName());

        tvh_nameAbility1 = (TextView)findViewById(R.id.abilityName1);
        tvh_nameAbility1.setText(heroData.getAbilities().get(1).getName());

        tvh_nameAbility2 = (TextView)findViewById(R.id.abilityName2);
        tvh_nameAbility2.setText(heroData.getAbilities().get(2).getName());

        tvh_nameAbility3 = (TextView)findViewById(R.id.abilityName3);
        tvh_nameAbility3.setText(heroData.getAbilities().get(3).getName());

        tvh_nameAbility4 = (TextView)findViewById(R.id.abilityName4);
        if(heroData.getAbilities().size() > 4) tvh_nameAbility4.setText(heroData.getAbilities().get(4).getName());

        tvh_ability0 = (TextView)findViewById(R.id.heroAbility0);
        tvh_ability0.setText(heroData.getAbilities().get(0).getDescription());

        tvh_ability1 = (TextView)findViewById(R.id.heroAbility1);
        tvh_ability1.setText(heroData.getAbilities().get(1).getDescription());

        tvh_ability2 = (TextView)findViewById(R.id.heroAbility2);
        tvh_ability2.setText(heroData.getAbilities().get(2).getDescription());

        tvh_ability3 = (TextView)findViewById(R.id.heroAbility3);
        tvh_ability3.setText(heroData.getAbilities().get(3).getDescription());

        tvh_ability4 = (TextView)findViewById(R.id.heroAbility4);
        if(heroData.getAbilities().size() > 4) tvh_ability4.setText(heroData.getAbilities().get(4).getDescription());

        hero_portrait = (ImageView)findViewById(R.id.mapPortrait);
        int id = getResources().getIdentifier("h" + (heroData.getId() - 1),"drawable", getPackageName());
        Drawable drawable = getResources().getDrawable(id);
        hero_portrait.setImageDrawable(drawable);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
