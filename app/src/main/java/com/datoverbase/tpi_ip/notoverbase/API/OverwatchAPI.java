package com.datoverbase.tpi_ip.notoverbase.API;

import com.datoverbase.tpi_ip.notoverbase.Data.Hero.HeroDetailData;
import com.datoverbase.tpi_ip.notoverbase.Data.Hero.HeroResponse;
import com.datoverbase.tpi_ip.notoverbase.Data.Map.MapDetailData;
import com.datoverbase.tpi_ip.notoverbase.Data.Map.MapResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface OverwatchAPI
{
    //Définition de l'url de base de l'api à utiliser
    String BASE_URL = "https://overwatch-api.net/api/v1/";


    @GET("hero")
    Call<HeroResponse> getHeroes(); //récupère la liste des heros

    @GET("hero/{heroID}")
    Call<HeroDetailData> getHeroID(@Path("heroID") int heroID); //récupère les details d'un hero spécifique

    @GET("map")
    Call<MapResponse> getMaps(); //récupère la liste des maps

    @GET("map/{mapID}")
    Call<MapDetailData> getMapID(@Path("mapID") int mapID);// récupère les details d'une map spécifique
}