package com.datoverbase.tpi_ip.notoverbase.Data.Player;

import java.util.Map;
import java.util.HashMap;
import android.os.Parcel;
import android.os.Parcelable;

public class PlayerStats implements Parcelable
{

    private Us us;
    private Eu eu;
    private Kr kr;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    public final static Parcelable.Creator<PlayerStats> CREATOR = new Creator<PlayerStats>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PlayerStats createFromParcel(Parcel in) {
            PlayerStats instance = new PlayerStats();
            instance.us = ((Us) in.readValue((Us.class.getClassLoader())));
            instance.eu = ((Eu) in.readValue((Eu.class.getClassLoader())));
            instance.kr = ((Kr) in.readValue((Kr.class.getClassLoader())));
            instance.additionalProperties = ((Map<String, Object> ) in.readValue((Map.class.getClassLoader())));
            return instance;
        }

        public PlayerStats[] newArray(int size) {
            return (new PlayerStats[size]);
        }

    }
            ;

    public Us getUs() {
        return us;
    }

    public void setUs(Us us) {
        this.us = us;
    }

    public Eu getEu() {
        return eu;
    }

    public void setEu(Eu eu) {
        this.eu = eu;
    }

    public Kr getKr() {
        return kr;
    }

    public void setKr(Kr kr) {
        this.kr = kr;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(us);
        dest.writeValue(eu);
        dest.writeValue(kr);
        dest.writeValue(additionalProperties);
    }

    public int describeContents() {
        return 0;
    }

}

