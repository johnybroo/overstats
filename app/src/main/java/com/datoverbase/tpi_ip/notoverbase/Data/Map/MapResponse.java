package com.datoverbase.tpi_ip.notoverbase.Data.Map;

import java.util.List;

public class MapResponse {

    private List<MapData> data = null;

    public List<MapData> getData() {
        return data;
    }

    public void setData(List<MapData> data) {
        this.data = data;
    }
}
