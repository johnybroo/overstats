package com.datoverbase.tpi_ip.notoverbase.Data.Player;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class Stats implements Parcelable
{
    private Competitive competitive;
    private Quickplay quickplay;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    public final static Creator<Stats> CREATOR = new Creator<Stats>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Stats createFromParcel(Parcel in) {
            Stats instance = new Stats();
            instance.competitive = ((Competitive) in.readValue((Competitive.class.getClassLoader())));
            instance.quickplay = ((Quickplay) in.readValue((Quickplay.class.getClassLoader())));
            instance.additionalProperties = ((Map<String, Object> ) in.readValue((Map.class.getClassLoader())));
            return instance;
        }

        public Stats[] newArray(int size) {
            return (new Stats[size]);
        }

    }
            ;

    public Competitive getCompetitive() {
        return competitive;
    }

    public void setCompetitive(Competitive competitive) {
        this.competitive = competitive;
    }

    public Quickplay getQuickplay() {
        return quickplay;
    }

    public void setQuickplay(Quickplay quickplay) {
        this.quickplay = quickplay;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(competitive);
        dest.writeValue(quickplay);
        dest.writeValue(additionalProperties);
    }

    public int describeContents() {
        return 0;
    }

}
