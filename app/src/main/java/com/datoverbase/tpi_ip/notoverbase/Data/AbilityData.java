package com.datoverbase.tpi_ip.notoverbase.Data;

import android.os.Parcel;
import android.os.Parcelable;

public class AbilityData implements Parcelable{
    private int id;
    private String name;
    private String description;
    private boolean is_ultimate;
    private String url;

    public AbilityData(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        is_ultimate = in.readByte() != 0;
        url = in.readString();
    }

    public AbilityData(){}

    public static final Creator<AbilityData> CREATOR = new Creator<AbilityData>() {
        @Override
        public AbilityData createFromParcel(Parcel in) {
            return new AbilityData(in);
        }

        @Override
        public AbilityData[] newArray(int size) {
            return new AbilityData[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isIs_ultimate() {
        return is_ultimate;
    }

    public void setIs_ultimate(boolean is_ultimate) {
        this.is_ultimate = is_ultimate;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeByte((byte) (is_ultimate ? 1 : 0));
        dest.writeString(url);
    }
}
