package com.datoverbase.tpi_ip.notoverbase.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;

import com.datoverbase.tpi_ip.notoverbase.R;

public class PatchNotesActivity extends AppCompatActivity {

    Toolbar tb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patchnotes_activity);
        tb = (Toolbar) findViewById(R.id.tb);
        setSupportActionBar(tb);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        WebView wvPatchNotes = (WebView) findViewById(R.id.wvPatchNotes);

        //Chargement de la page web à afficher
        wvPatchNotes.loadUrl("https://playoverwatch.com/en-us/game/patch-notes/pc/");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}