package com.datoverbase.tpi_ip.notoverbase.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.datoverbase.tpi_ip.notoverbase.API.ClientServiceGenerator;
import com.datoverbase.tpi_ip.notoverbase.API.OverwatchAPI;
import com.datoverbase.tpi_ip.notoverbase.Data.DataManager;
import com.datoverbase.tpi_ip.notoverbase.R;

import java.io.IOException;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getData();
    }

    void getData()
    {
        //On récupère les données dans un thread séparé pour ne pas bloquer l'interface pendant le chargement
        new Thread(new Runnable() {
            @Override
            public void run() {
                OverwatchAPI clientOverwatchAPI = ClientServiceGenerator.createService(OverwatchAPI.class, OverwatchAPI.BASE_URL);
                try
                {
                    DataManager.getInstance().setHeroeasData(clientOverwatchAPI.getHeroes().execute().body().getData());
                    DataManager.getInstance().setMapsData(clientOverwatchAPI.getMaps().execute().body().getData());
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent); //Une fois les données recuperées on lance l'activité principale
                finish();
            }
        }).start();
    }

}
