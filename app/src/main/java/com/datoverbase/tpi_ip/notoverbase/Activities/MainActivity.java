package com.datoverbase.tpi_ip.notoverbase.Activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.datoverbase.tpi_ip.notoverbase.R;

public class MainActivity extends AppCompatActivity {

    Toolbar tb;
    ActionBarDrawerToggle ab;
    CardView cv_patchnotes, cv_heroes, cv_maps, cv_events;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        tb = (Toolbar) findViewById(R.id.tb);
        setSupportActionBar(tb);

        cv_patchnotes = (CardView) findViewById(R.id.patch_notes_cv);
        cv_heroes = (CardView) findViewById(R.id.heroes_cv);
        cv_maps = (CardView) findViewById(R.id.maps_cv);
        cv_events = (CardView) findViewById(R.id.events_cv);

        cv_patchnotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PatchNotesActivity.class);
                startActivity(intent);
            }
        });
        cv_heroes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListHeroesActivity.class);
                startActivity(intent);
            }
        });
        cv_maps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListMapsActivity.class);
                startActivity(intent);
            }
        });
        cv_events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListEventsActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        ab.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate le menu, ajoute des options à l'Action Bar si cette dernière est présente
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Lorsqu'une option de notre Toolbar est sélectionnée
        Intent myIntent;

        switch (item.getItemId()) {
            case R.id.stats:
                myIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(myIntent);
                return true;
        }
        return true;
    }
}
