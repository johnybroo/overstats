package com.datoverbase.tpi_ip.notoverbase.Tracker;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.datoverbase.tpi_ip.notoverbase.Fragments.CompetitiveFragment;
import com.datoverbase.tpi_ip.notoverbase.Fragments.QuickplayFragment;
import com.datoverbase.tpi_ip.notoverbase.R;

public class Statistics extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tl;
    Toolbar tb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics);
        tb = (Toolbar) findViewById(R.id.tb);
        setSupportActionBar(tb);
        viewPager = (ViewPager) findViewById(R.id.vp);
        tl = (TabLayout) findViewById(R.id.tl);
        Statistics.MyAdapter MyAdapter = new Statistics.MyAdapter(getSupportFragmentManager());
        viewPager.setAdapter(MyAdapter);
        tl.setupWithViewPager(viewPager);
    }


    class MyAdapter extends FragmentStatePagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment f = null;
            if (position == 0) {
                f = new QuickplayFragment();
            }
            if (position == 1) {
                f = new CompetitiveFragment();
            }
            return f;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String name = null;
            if (position == 0) {
                name = "Quickplay";
            }
            if (position == 1) {
                name = "Competitive";
            }
            return name;
        }
    }
}