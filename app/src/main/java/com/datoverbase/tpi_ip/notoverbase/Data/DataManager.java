package com.datoverbase.tpi_ip.notoverbase.Data;

import com.datoverbase.tpi_ip.notoverbase.Data.Hero.HeroData;
import com.datoverbase.tpi_ip.notoverbase.Data.Map.MapData;
import com.datoverbase.tpi_ip.notoverbase.Data.Player.Stats;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataManager {

    private static DataManager instance = new DataManager();

    public static DataManager getInstance()
    {
        return instance;
    }

    private List<HeroData> heroeasData;
    private List<MapData> mapsData;

    private Map<String, String> mapsDescriptions;

    private  String pseudo;
    private Stats playerStats;

    private DataManager()
    {
        mapsDescriptions = new HashMap<>();
        mapsDescriptions.put("Dorado", "Dorado");
        mapsDescriptions.put("Eichenwalde", "Eichenwalde");
        mapsDescriptions.put("Estádio das Rãs", "EstadiodasRas");
        mapsDescriptions.put("Hanamura", "Hanamura");
        mapsDescriptions.put("Hollywood", "Hollywood");
        mapsDescriptions.put("Ilios", "Ilios");
        mapsDescriptions.put("King's Row", "KingsRow");
        mapsDescriptions.put("Lijiang Tower", "LijiangTower");
        mapsDescriptions.put("Route 66", "Route66");
        mapsDescriptions.put("Numbani", "Numbani");
        mapsDescriptions.put("Nepal", "Nepal");
        mapsDescriptions.put("Temple of Anubis", "TempleofAnubis");
        mapsDescriptions.put("Volskaya Industries", "VolskayaIndustries");
        mapsDescriptions.put("Watchpoint: Gibraltar", "Gibraltar");
        mapsDescriptions.put("Ecopoint: Antarctica", "EcopointAntarctica");

    }

    public Map<String, String> getMapsDescriptions() {
        return mapsDescriptions;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public Stats getPlayerStats() {
        return playerStats;
    }

    public void setPlayerStats(Stats playerStats) {
        this.playerStats = playerStats;
    }

    public List<HeroData> getHeroeasData() {
        return heroeasData;
    }

    public void setHeroeasData(List<HeroData> heroeasData) {
        this.heroeasData = heroeasData;
    }

    public List<MapData> getMapsData() {
        return mapsData;
    }

    public void setMapsData(List<MapData> mapsData) {
        this.mapsData = mapsData;
    }
}
