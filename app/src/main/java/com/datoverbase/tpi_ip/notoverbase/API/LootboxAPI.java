package com.datoverbase.tpi_ip.notoverbase.API;

import com.datoverbase.tpi_ip.notoverbase.Data.PatchNote.PatchNoteResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface LootboxAPI
{
    //Définition de l'url de base à utiliser
    String BASE_URL = "https://api.lootbox.eu/";

    @GET("patch_notes")
    Call<PatchNoteResponse> getPatchNotes();
}