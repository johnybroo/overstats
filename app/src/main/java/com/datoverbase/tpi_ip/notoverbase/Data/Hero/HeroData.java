package com.datoverbase.tpi_ip.notoverbase.Data.Hero;

import android.os.Parcel;
import android.os.Parcelable;

public class HeroData implements Parcelable{

    private int id;
    private String name;
    private String description;
    private int health;
    private int armour;
    private int shield;
    private String real_name;
    private int age;
    private String affiliation;
    private String base_of_operations;
    private int difficulty;
    private String url;

    public HeroData()
    {

    }

    protected HeroData(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        health = in.readInt();
        armour = in.readInt();
        shield = in.readInt();
        real_name = in.readString();
        age = in.readInt();
        affiliation = in.readString();
        base_of_operations = in.readString();
        difficulty = in.readInt();
        url = in.readString();
    }

    public static final Creator<HeroData> CREATOR = new Creator<HeroData>() {
        @Override
        public HeroData createFromParcel(Parcel in) {
            return new HeroData(in);
        }

        @Override
        public HeroData[] newArray(int size) {
            return new HeroData[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public Integer getArmour() {
        return armour;
    }

    public void setArmour(int armour) {
        this.armour = armour;
    }

    public Integer getShield() {
        return shield;
    }

    public void setShield(int shield) {
        this.shield = shield;
    }

    public String getReal_name() {
        return real_name;
    }

    public void setReal_name(String real_name) {
        this.real_name = real_name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getBase_of_operations() {
        return base_of_operations;
    }

    public void setBase_of_operations(String base_of_operations) {
        this.base_of_operations = base_of_operations;
    }

    public Integer getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeInt(health);
        dest.writeInt(armour);
        dest.writeInt(shield);
        dest.writeString(real_name);
        dest.writeInt(age);
        dest.writeString(affiliation);
        dest.writeString(base_of_operations);
        dest.writeInt(difficulty);
        dest.writeString(url);
    }
}
