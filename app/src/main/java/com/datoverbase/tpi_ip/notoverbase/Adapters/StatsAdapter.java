package com.datoverbase.tpi_ip.notoverbase.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.datoverbase.tpi_ip.notoverbase.Fragments.CompetitiveFragment;
import com.datoverbase.tpi_ip.notoverbase.Fragments.QuickplayFragment;

public class StatsAdapter extends FragmentStatePagerAdapter {


    public StatsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment f = null;

        if (position == 0) {
            f = new QuickplayFragment();
        }
        if (position == 1) {
            f = new CompetitiveFragment();
        }
        return f;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String name = null;
        if (position == 0) {
            name = "Quickplay";
        }
        if (position == 1) {
            name = "Competitive";
        }
        return name;
    }
}
