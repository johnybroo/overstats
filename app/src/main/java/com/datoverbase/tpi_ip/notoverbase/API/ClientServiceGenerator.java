package com.datoverbase.tpi_ip.notoverbase.API;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//Classe qui permet de creer un client pour executer les requetes definie dans l'interface fournie
public class ClientServiceGenerator{
    private static OkHttpClient httpClient = new OkHttpClient();

    public static <S> S createService(Class<S> serviceClass, String URL) {
        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(URL)
                        .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.client(httpClient).build();
        return retrofit.create(serviceClass);
    }
}
