package com.datoverbase.tpi_ip.notoverbase.Activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.datoverbase.tpi_ip.notoverbase.Data.DataManager;
import com.datoverbase.tpi_ip.notoverbase.Data.Map.MapDetailData;
import com.datoverbase.tpi_ip.notoverbase.R;

public class DetailMapActivity extends AppCompatActivity {

    Toolbar tb;
    MapDetailData mapData;
    TextView tvm_name, tvm_location, tvm_description;
    ImageView map_portrait;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        tb = (Toolbar) findViewById(R.id.tb);
        setSupportActionBar(tb);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mapData = getIntent().getParcelableExtra("mapData"); //Recuperation des info de la map clickée

        setMapData();
    }

    private void setMapData()
    {
        tvm_name = (TextView)findViewById(R.id.mapName);
        tvm_name.setText(mapData.getName());

        tvm_location = (TextView)findViewById(R.id.mapLocation);
        tvm_location.setText(mapData.getLocation());

        tvm_description = (TextView)findViewById(R.id.mapDescription);
        String mapDescriptionName = DataManager.getInstance().getMapsDescriptions().get(mapData.getName());
        Log.e("MAP", mapDescriptionName);
        int descriptionID = getResources().getIdentifier(mapDescriptionName, "string", getPackageName());
        tvm_description.setText(getResources().getString(descriptionID));

        map_portrait = (ImageView)findViewById(R.id.mapPortrait);
        int id = getResources().getIdentifier("m" + (mapData.getId() - 1),"drawable", getPackageName());
        Log.e("lol", "h" + (mapData.getId() -1) + ".png");
        Drawable drawable = getResources().getDrawable(id);
        map_portrait.setImageDrawable(drawable);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
