package com.datoverbase.tpi_ip.notoverbase.Data.Player;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class Competitive implements Parcelable
{

    private Overall_stats overall_stats;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    public final static Creator<Competitive> CREATOR = new Creator<Competitive>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Competitive createFromParcel(Parcel in) {
            Competitive instance = new Competitive();
            instance.overall_stats = ((Overall_stats) in.readValue((Overall_stats.class.getClassLoader())));
            instance.additionalProperties = ((Map<String, Object> ) in.readValue((Map.class.getClassLoader())));
            return instance;
        }

        public Competitive[] newArray(int size) {
            return (new Competitive[size]);
        }

    }
            ;

    public Overall_stats getOverall_stats() {
        return overall_stats;
    }

    public void setOverall_stats(Overall_stats overall_stats) {
        this.overall_stats = overall_stats;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(overall_stats);
        dest.writeValue(additionalProperties);
    }

    public int describeContents() {
        return 0;
    }

}
