package com.datoverbase.tpi_ip.notoverbase.API;

import com.datoverbase.tpi_ip.notoverbase.Data.Player.PlayerStats;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface OWAPI {
    String BASE_URL = "https://owapi.net/api/v3/u/";

    @GET("{BTag}/stats")
    Call<PlayerStats> getPlayerStats(@Path("BTag") String BTag);

}